import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TableQuestionComponent } from './table-question/table-question.component';
import { QuestionComponent } from './question/question.component';
import { FlashCardComponent } from './flash-card/flash-card.component';
import { AdminComponent } from './admin/admin.component';
import { ReadMeComponent } from './read-me/read-me.component';
import { QuestionHelperComponent } from './question-helper/question-helper.component';

@NgModule({
  declarations: [
    AppComponent,
    TableQuestionComponent,
    QuestionComponent,
    FlashCardComponent,
    AdminComponent,
    ReadMeComponent,
    QuestionHelperComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
