import { Component, Output, EventEmitter } from '@angular/core';
import { QuestionsService } from '../questions.service';
import { Question } from '../Question';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent {

  questionsList:  any;
  answerChosen: boolean = false;
  correctAnswer: string;
  correctChosen: boolean;
  answerStates;
  answers: Array<string>;
  questions: Array<string>;
  @Output() answerEvent = new EventEmitter<string>();

  constructor(private questionsService: QuestionsService) { }

  parseQuestions(){
    this.questionsService.getQuestions()
    .subscribe((question: Question[]) => {
      this.questionsList = question;
      this.questions = [];
      var i;
      for(i = 0; i < this.questionsList.length; i++){
        this.questions.push(this.questionsList[i].question);
      }
    });
  }


  // Generates the possible options for answers and randomises the array
  generateOptions(qnum:number): void{
    this.correctAnswer = "";
    this.correctChosen = false;
    this.answerChosen = false;
    this.answers = [];
    this.answerStates = {};
    var i;
    var wrong: Array<string> = [];
    var correct: string = "";
    wrong = (this.questionsList[qnum].wrong);
    correct = (this.questionsList[qnum].correct);
    this.correctAnswer = correct;
    for (i = 0; i < 3; i++){
      this.answers.push(wrong[i]);
    }
    var rand = Math.floor(Math.random() * 4);
    this.answers.push(correct);
    this.answers[rand] = correct;
    var wrongnum = 0;
    for (i = 0; i < 4; i++){
      if(i == rand)
        continue;
      else{
        this.answers[i] = wrong[wrongnum];
        wrongnum++;
      }
    }
    for(i = 0; i < 4; i++){
      this.answerStates[this.answers[i]] = false;
    }
  }

  // This function is ran when the user clicks an answer
  // answer chosen will be emitted to flash cards component,
  // so that appropriate css can be coded for wrong/correct answers
  answerClick(chosen: string){
    this.answerStates[chosen] = !this.answerStates[chosen];
    this.answerChosen = true;
    for(var s in this.answerStates){
      if(this.answerStates[s] == true){
        if(s == this.correctAnswer){
          this.correctChosen = true;
        }
        this.answerEvent.emit(s);
      }
    }
  }

}
