import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Question } from './Question';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  private BASE_URL = environment.API_URL;
  constructor(private http: HttpClient) { }

  getQuestions(): Observable<Question[]> {
    return this.http.get<Question[]>(`${this.BASE_URL}/questions`);
  }

  storeQuestion(id: number, question: string, correct: string, wrong: Array<string>, difficulty: string): Observable<Question> {
    return this.http.post<Question>(`${this.BASE_URL}/questions`,
    {id, question, correct, wrong, difficulty});
  }
}
