export interface Question {
    id: number;
    question: string;
    correct: string;
    wrong: Array<string>;
    difficulty: string;
}