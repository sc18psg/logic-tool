export interface FlashCard {
    unansweredDeck: Array<string>;
    wrongDeck: Array<string>;
    correctDeck: Array<string>;
}