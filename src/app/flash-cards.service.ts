import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FlashCard } from './FlashCard';

@Injectable({
  providedIn: 'root'
})
export class FlashCardsService {

  private BASE_URL = environment.API_URL;

  constructor(private http: HttpClient) { }

  getFlashCard(id: string): Observable<FlashCard> {
    return this.http.get<FlashCard>(`${this.BASE_URL}/flash-card/${id}`);
  }

  storeFlashCards(unansweredDeck: Array<string>, wrongDeck: Array<string>, correctDeck: Array<string>): Observable<FlashCard> {
    return this.http.post<FlashCard>(`${this.BASE_URL}/flash-card`,
    {unansweredDeck, wrongDeck, correctDeck});
  }
}
