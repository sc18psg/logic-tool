import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../questions.service';

import { Question } from '../Question';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  allQuestions: any;
  username: string = "";
  password: string = "";
  loginPassed: boolean = false;
  errorMsg: string = "";
  message: string = "";
  
  question: string = "";
  correct: string = "";
  wrong1: string = "";
  wrong2: string = "";
  wrong3: string = "";
  difficulty: string = "";

  constructor(private questionsService: QuestionsService) {
  }

  ngOnInit(): void {
    this.loginPassed = false;
  }

  addQuestion(): void {
    if(!this.correct || !this.difficulty || !this.wrong1 || !this.wrong2 || !this.wrong3 || !this.question){
      this.errorMsg = "All fields must be filled in!";
      this.message = "";
    }
    else{
      var wrong: Array<string> = [];
      wrong.push(this.wrong1);
      wrong.push(this.wrong2);
      wrong.push(this.wrong3);
      this.errorMsg = "";
      this.questionsService.getQuestions()
      .subscribe((question: Question[]) => {
        this.allQuestions = question;
        if(question){
          this.questionsService.storeQuestion(question.length, this.question, this.correct, wrong, this.difficulty)
          .subscribe((question: any) => {
            this.message = "Successfully stored Question!";
            console.log("Successfully stored Question!");
            console.log(question);
            this.question = "";
            this.correct = "";
            this.wrong1 = "";
            this.wrong2 = "";
            this.wrong3 = "";
            this.difficulty = "";
          });
        }
        else{
          this.questionsService.storeQuestion(0, this.question, this.correct, wrong, this.difficulty)
          .subscribe((question: any) => {
            this.message = "Successfully stored Question!";
            console.log("Successfully stored Question!");
            console.log(question);
            this.question = "";
            this.correct = "";
            this.wrong1 = "";
            this.wrong2 = "";
            this.wrong3 = "";
            this.difficulty = "";
          });
        }
     });
    }
  }

validateLogin() {
    if(this.username == "admin"){
      if(this.password == "?&D9Ecmp[sj9sQ.y"){
        this.loginPassed = true;
        this.errorMsg = "";
      }
      else{
        this.errorMsg = "Incorrect password!"
      }
    }
    else{
      this.errorMsg = "Incorrect username!"
    }
  }
}
