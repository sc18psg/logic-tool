import { Component, OnInit, ViewChild } from '@angular/core';
import { Formula } from 'logically';

@Component({
  selector: 'app-syntax',
  templateUrl: './table-question.component.html',
  styleUrls: ['./table-question.component.scss'],
  
})
export class TableQuestionComponent implements OnInit {

  //@ViewChild(ParseTreeComponent) child:ParseTreeComponent;

  truth: any;
  statement:any;
  header:any;
  randString: string = "";
  value: string = "";
  answerClicked: boolean = false;

  question: string = ``;
  randQuestionFormula: string = "";
  atomicVariables: any;
  variableStates: any = {};
  hintRequired: boolean = false;
  userAnswer: string = "";
  correctAnswer: string;
  rowNum: number = -1;

  currentScr: number = 0;
  totalScr: number = 0;

  constructor() {
    
   }

  ngOnInit(): void {
    this.generateRandomQuestion();
  }

  generateRandomQuestion(): void {
    this.hintRequired = false;
    this.answerClicked = false;
    this.userAnswer = "";
    this.rowNum = -1;
    // Choose random number between 1 and 3 inclusive for the 3 different
    // types of questions
    const a: number = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
    // All questions will use a randomly generated propositional formula
    // and its table
    let rdmParams = { nVariables : 3, difficulty : 0.3, minRecursionLevel : 1, maxRecursionLevel : 3  }
    var rdm = Formula.generateRandomFormula( rdmParams );
    this.randQuestionFormula = rdm.cleansedFormulaString;
    this.atomicVariables = Formula.getAtomicVariables(this.randQuestionFormula);
    this.getTable(this.randQuestionFormula);
    if(a == 1){
      var i;
      for(i = 0; i < this.atomicVariables.length; i++){
        //  randomly assign true/false values to each atomic variable
        const b: number = Math.floor(Math.random() * (2 - 1 + 1)) + 1;
        //  if a turns out to be 1, label current variable as true
        if(b == 1){
          this.variableStates[this.atomicVariables[i]] = true
        }
        //  else, label current variable as false
        else{
          this.variableStates[this.atomicVariables[i]] = false
        }
      }
      this.question = `If the atomic variables in the expression
"${this.randQuestionFormula}"
had the following truth assignments:
${JSON.stringify(this.variableStates)}
what would be the truth value?`;
    }
    else if(a == 2){
      this.question = `Would the expression:
"${this.randQuestionFormula}"
be a tautology, a contingency, or a contradiction?`
    }
    else{
      this.question =`What would be the outermost operation in the formula:
"${this.randQuestionFormula}"?`
    }
    this.sortTable(a);
  }

  checkAnswer(): void{ 
    // IMPLEMENT FEATURE THAT SHOWS CORRECT ANSWER (REFILL TABLE)
    if(!this.userAnswer || this.answerClicked)
      return;
    this.answerClicked = true;
    if(this.userAnswer == this.correctAnswer.toString()){
      this.currentScr++;
    }
    this.getTable(this.randQuestionFormula);
    this.totalScr++;
  }

  hintButton(){
    this.hintRequired = !this.hintRequired;
  }

  getTable(statement:string){
    var tbl = Formula.generateTruthTable(statement);
    this.truth = tbl;
    this.header = Formula.generateTruthTableHeaders(statement);
  }

  //  Removes appropriate information from hint table
  //  as well as stores correct answer for comparison
  sortTable(a: number): void{
    //  truth assignments question
    if(a == 1){
      var tblLength = this.truth[0].length;
      var count: number;
      count = 0;
      var i;
      var j;
      for(i = 0; i < this.truth.length; i++){
        for(j = 0; j < this.atomicVariables.length; j++){
          if(this.variableStates[this.atomicVariables[j]] == this.truth[i][j]){
            count++;
            if(count == this.atomicVariables.length){
              this.rowNum = i;
              // store hide the answer
              this.correctAnswer = this.truth[i][tblLength - 1];
              this.truth[i][tblLength - 1] = "";
              return;
            }
          }
        }
        count = 0;
      }
    }
    //  tautology/contradiction/contingency question
    else if(a == 2){
      var results: Array<boolean> = [];
      var i;
      var j;
      for(i = 0; i < this.truth.length; i++){
        results.push(this.truth[i][this.truth[0].length - 1]);
        this.truth[i][this.truth[0].length - 1] = "";
      }

      //  Code snippet taken from Rajnish Katharotiya of dev.to/
      const allEqual = arr => arr.every(val => val === arr[0]);

      if(allEqual(results)){
        if(results[0] == true)
          this.correctAnswer = "tautology";
        else
          this.correctAnswer = "contradiction"
      }
      else{
        this.correctAnswer = "contingency";
      }
    }
    //  Outermost operator question
    else {
      this.correctAnswer = Formula.findMainOp(this.randQuestionFormula).operator
      var i;
      for(i = this.atomicVariables.length; i < this.header.length - 1; i++){
        this.header[i] = "";
      }
    }
  }

  getInput(event: any){
    this.value = event.target.value;
    if(Formula.isWFFString(this.value)){
      var formula = new Formula(this.value);
      this.getTable(formula.cleansedFormulaString);
    }
  }
}
