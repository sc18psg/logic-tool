import { Component, OnInit } from '@angular/core';
import { Formula } from 'logically';

@Component({
  selector: 'app-question-helper',
  templateUrl: './question-helper.component.html',
  styleUrls: ['./question-helper.component.scss']
})
export class QuestionHelperComponent implements OnInit {

  randString: string = "";
  header: any;
  table: any;
  correctAnswer: string;
  errorMsg: string;
  parseTree: Array<string>;

  constructor() { }

  ngOnInit(): void {
  }

  getTable(): void {
    try {
      this.table = Formula.generateTruthTable(this.randString);
      this.errorMsg = "";
      this.header = Formula.generateTruthTableHeaders(this.randString);
      var results: Array<boolean> = [];
      var i;
      for(i = 0; i < this.table.length; i++){
        results.push(this.table[i][this.table[0].length - 1]);
        //this.table[i][this.table[0].length - 1] = "";
      }
  
      //  Code snippet taken from Rajnish Katharotiya of dev.to/
      const allEqual = arr => arr.every(val => val === arr[0]);
  
      if(allEqual(results)){
        if(results[0] == true)
          this.correctAnswer = "Tautology because all outputs are true";
        else
          this.correctAnswer = "Contradiction because all outputs are false"
      }
      else{
        this.correctAnswer = "Contingency because the outputs are different";
      }
    }
    catch(err){
      this.errorMsg = "Error: formula is not well formed. Please ensure you are using the correct symbols!";
      this.table = [];
      this.header = [];
      this.correctAnswer = "";
    }
  
  }

  getParseTree(): void{
    if(this.header){
      this.parseTree = [];
      var temp = this.header.slice().reverse();
      var i;
      for( i = 0; i < temp.length; i++){
        var op = Formula.findMainOp(temp[i]).operator
        if( op != null){
          this.parseTree.push(op);
        }
      }
    }
  }

  randomClick(){
    let rdmParams = { nVariables : 3, difficulty : 0.3, minRecursionLevel : 1, maxRecursionLevel : 3  }
    var rdm = Formula.generateRandomFormula( rdmParams );
    this.randString = rdm.cleansedFormulaString;
    this.getTable();
    this.getParseTree();
    this.errorMsg = "";
  }
  
  valueEntered(){
    this.getTable();
    this.getParseTree();
  }

}
