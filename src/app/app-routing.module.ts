import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { FlashCardComponent } from './flash-card/flash-card.component';
import { QuestionHelperComponent } from './question-helper/question-helper.component';
import { QuestionComponent } from './question/question.component';
import { ReadMeComponent } from './read-me/read-me.component';
import { TableQuestionComponent } from './table-question/table-question.component';

const routes: Routes = [
  { path: 'tablequestions', component: TableQuestionComponent },
  { path: 'helper', component: QuestionHelperComponent },
  { path: 'question', component:QuestionComponent },
  { path: 'flash-card', component: FlashCardComponent },
  { path: 'admin', component: AdminComponent },
  { path: '', component: ReadMeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
