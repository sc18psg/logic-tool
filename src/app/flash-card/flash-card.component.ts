import { Component, ViewChild, OnInit } from '@angular/core';
import { FlashCardsService } from '../flash-cards.service';
import { FlashCard } from '../FlashCard';
import { QuestionComponent } from '../question/question.component';

@Component({
  selector: 'app-flash-card',
  templateUrl: './flash-card.component.html',
  styleUrls: ['./flash-card.component.scss']
})
export class FlashCardComponent implements OnInit {
  @ViewChild(QuestionComponent) child:QuestionComponent;

  showModal: boolean = false;
  successMsg: string;
  errorMsg: string;
  error: string;
  idString: string = "";
  savedID: string = "";

  haveStarted: boolean = false;
  isMenu: boolean = false;
  correctNum: number = 0;
  wrongNum: number = 0;
  unansweredNum: number = 0;

  currentQuestion: string = "";
  correctAnswer: string = '';
  questionNumber: number = 0;

  unansweredDeck: Array<string> = [];
  correctDeck: Array<string> = [];
  wrongDeck: Array<string> = [];
  currentDeck: any = [];

  constructor(private flashCardsService: FlashCardsService) { }

  ngOnInit(): void {
    //this.getFlashCards(0);
    this.savedID = "";
  }
  ngAfterViewInit() {
      this.child.parseQuestions();
  }

  //  Gets flash card with given ID from database
  //  Will give error on page if not successful

  getFlashCards(id: string): void {
    this.flashCardsService.getFlashCard(id)
    .subscribe((flashCard: FlashCard) => {
      this.unansweredDeck = flashCard.unansweredDeck;
      this.wrongDeck = flashCard.wrongDeck;
      this.correctDeck = flashCard.correctDeck;
      this.cardMenu();

    },
    (error: ErrorEvent) => {
      this.errorMsg = "Could not find any Flash Card with given ID!";
    });
  }

  //  This function is used to start the flash card component
  //  If num == 1, this means the user has put in a FlashCard ID,
  //  num == 0 otherwise.

  // try usin .then() and return
  // update pre existing flash cards with new
  // added questions, put them into unanswered deck

  startFlashCards(num: number): void {
    if(num == 1){
      this.getFlashCards(this.idString);
    }
    else{
      var i;
      for(i = 0; i < this.child.questionsList.length; i++){
        this.unansweredDeck.push(this.child.questionsList[i]);
      }
      this.currentDeck = this.unansweredDeck;
      this.cardMenu();
    }
  }

  resumeFlashCards(num: number): void {
    if(num == 0)
      this.currentDeck = this.correctDeck;
    else if(num == 1)
      this.currentDeck = this.wrongDeck;
    else if(num == 2)
      this.currentDeck = this.unansweredDeck;

    if(this.currentDeck.length == 0)
      return;
    this.isMenu = false;
    this.haveStarted = true;
    this.getQuestion();
  }

  cardMenu(): void {
    this.idString = "";
    this.errorMsg = "";
    this.isMenu = true;
    this.haveStarted = false;
    this.correctAnswer = '';
    this.child.answers = [];
    this.correctNum = this.correctDeck.length;
    this.unansweredNum = this.unansweredDeck.length;
    this.wrongNum = this.wrongDeck.length;
  }

  getQuestion(): void{
    this.currentQuestion = this.currentDeck[this.questionNumber].question;
    this.child.generateOptions(this.currentDeck[this.questionNumber].id);
  }
  
  getAnswer(ans: string):void {
    var details = this.currentDeck[this.questionNumber];
    this.currentDeck.splice(0, 1);
    if(this.child.correctChosen){
      this.correctDeck.push(details);
    }
    else{
      this.wrongDeck.push(details);
    }
    this.correctAnswer = "Answer: " + this.child.correctAnswer;
  }

  nextQuestion(): void {
    if(this.currentDeck.length == 0){
      this.cardMenu();
    }
    else{
      //this.questionNumber++;
      this.getQuestion();
      this.correctAnswer = '';
    }
  }

  closeModal(): void {
    this.showModal = false;
    this.haveStarted = false;
    this.isMenu = false;
    this.savedID = "";
  }
  
  exitFlashCards(): void {
    this.flashCardsService.storeFlashCards(this.unansweredDeck, this.wrongDeck, this.correctDeck)
      .subscribe((createdFlash: any) => {
        this.correctDeck = [];
        this.wrongDeck = [];
        this.unansweredDeck = [];
        this.savedID = createdFlash._id;
        this.successMsg = 'Flash cards successfully stored!'
      },
      (error: ErrorEvent) => {
        this.errorMsg = error.error.message;
      })
      this.showModal = true;
  }
}
