import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  name = 'Get Current Url Route Demo';
  currentRoute: string;

  constructor(private router: Router){
    //console.log(this.router.url);
  }
  ngOnInit(){
    //console.log(window.location.href);
  }
}

